<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Auth::routes();

  
Route::get('/', 'Auth\LoginController@ShowLoginForm')->middleware('guest');;


Route::group(['middleware' => ['auth']], function () {
    Route::get('/dashboard', function () {
        return view('administrator/dashboard/dashboard');  
    });
    
    Route::resource('/barang', 'barangController');
    Route::get('barang/tambah/stok', 'barangController@addStock')->name('add.stok');
    Route::post('barang/tambah/stok', 'barangController@storeStock')->name('barang.stok.store');
    Route::resource('/gudang', 'gudangController');
    Route::resource('/transaksi', 'transaksiController');
    Route::resource('/cart', 'CartController');
    Route::resource('/laporan', 'laporanController');
    Route::post('cart/add', 'CartController@add')->name('cart.add');
    Route::post('cart/remove', 'CartController@remove')->name('cart.remove');

    Route::get('/transaksi/print/{invoice}', 'transaksiController@generatePDF')->name('invoice.print');
    
    Route::get('/pelanggan', function () {
        return view('administrator/pelanggan');  
    });
});

Route::get('/home', 'HomeController@index')->name('home');
