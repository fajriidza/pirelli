<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Invoice</title>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <style>
        body{
            font-family:'Gill Sans', 'Gill Sans MT', Calibri, 'Trebuchet MS', sans-serif;
            color:#333;
            text-align:left;
            font-size:12px;
            margin:0;
        }
        table{
            border:1px solid #333;
            border-collapse:collapse;
        }
        td, tr, th{
            border:1px solid #333;
        }
        h4, p{
            margin:0px;
        }
    </style>
</head>
<body>
    <div class="container">
        <table>
            <thead>
                <tr>
                    <td colspan="6"><h2 style="margin-bottom:0px;margin-top:0px;">CV. RIDHO JAYA</h2><p>Jl. Sungai Kahayan No.73 RT.18 Tanah Patah Bengkulu</p></td>
                    <th>#{{$transaksi->invoice}}</th>
                </tr>
            </thead>
            <tbody>
                <tr>
                    <td colspan="6">
                        <table style="margin-top:15px;margin-bottom:15px;">
                            <tr>
                                <td width="225px">No INVOICE</td>
                                <td width="305px">{{$transaksi->invoice}}</td>
                            </tr>
                            <tr>
                                <td width="225px">NAMA TOKO</td>
                                <td width="305px">{{$transaksi->name}}</td>
                            </tr>
                            <tr>
                                <td width="225px">ALAMAT</td>
                                <td width="305px">{{$transaksi->address}}</td>
                            </tr>
                            <tr>
                                <td width="225px">TGL KIRIM BARANG</td>
                                <td width="305px">{{date('d, M Y' , strtotime($transaksi->created_at))}}</td>
                            </tr>
                            <tr>
                                <td width="225px">TGL JATUH TEMPO</td>
                                <td width="305px"></td>
                            </tr>

                       
                        </table>
                    </td>
                    <td></td>
                </tr>
                <tr class="text-center">
                    <th scope="col">Ukuran</th>
                    <th scope="col" width="150px">Pattern</th>
                    <th scope="col">QTY</th>
                    <th scope="col" width="100px">Harga Het</th>
                    <th scope="col" width="100px">Disc</th>
                    <th scope="col">Harga Set Discount</th>
                    <th scope="col">Total</th>
                  </tr>
                @foreach ($transaksi->transaksi_details as $item)
                <tr class="text-center">
                    <td>{{$item->product->ukuran}}</td>
                    <td>{{$item->product->pattern}}</td>
                    <td>{{$item->qty}}</td>
                    <td>{{'Rp '.number_format($item->product->het,0,',','.')}}</td>
                    @if($item->discount_one==null &&$item->discount_two==null)
                    <td>{{' '}}</td>
                    @elseif($item->discount_one!=null &&$item->discount_two==null)
                    <td>{{$item->discount_one.'%'}}</td>
                    @elseif($item->discount_one==null &&$item->discount_two!=null)
                    <td>{{$item->discount_two.'%'}}</td>
                    @elseif($item->discount_one!=null &&$item->discount_two!=null)
                    <td>{{$item->discount_one.'% +'.$item->discount_two.'%'}}</td>
                    @endif
                    <td>{{'Rp '.number_format($item->discounted_price,0,',','.')}}</td>
                    <td>{{'Rp '.number_format($item->amount,0,',','.')}}</td>
                </tr>
                 <tr>
                    <td colspan="5" style="border-left-style:hidden;border-bottom-style:hidden;border-right-style:hidden;">&nbsp;</td>
                    <td style="border-left-style:hidden;border-bottom-style:hidden;">Total</td>
                    <td>{{'Rp '.number_format($transaksi->amount,0,',','.')}}</td>
                </tr>
                @endforeach
            </tbody>
            <tfoot>
                <tr>
                    <td colspan="6" style="border-top-style:hidden;">
                        <table>
                            <tr>
                                <td width="300px">YANG MENERIMA BARANG</td>
                                <td width="230px">CV.RIDHO JAYA</td>
                            </tr>
                            <tr>
                                <td height="50px"></td>
                                <td height="50px"></td>
                            </tr>
                        </table>
                    </td>
                    <td></td>
                </tr>
            </tfoot>
        </table>
    </div>
</body>
</html>