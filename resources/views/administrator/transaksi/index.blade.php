@extends('administrator.header')
@section('isi')
@include('administrator.barang.flash-message')
<div class="container" style="padding-top:25px;">
    <div class="card">
        <div class="card-body">
          <h5 class="card-title">Transaksi
            <form method="get" action="" enctype="multipart/form-data">
              <div class="mb-3 form-inline" style="float:right"> 
                  <div class="form-group">
                      <div class="input-group">
                          <input type="text" name="search" class="form-control" placeholder="Cari Transaksi">
                          <div class="input-group-append">
                              <button type="submit" class="btn btn-secondary shadow-none">Cari</button>
                          </div>
                      </div>
                  </div>
              </div>
              </form>
          </h5>
          <table class="table table-striped table-bordered">
            <thead>
              <tr class="text-center">
                <th scope="col">Kode Transaksi</th>
                <th scope="col">Nama Pembeli</th>
                <th scope="col">Tanggal Pembelian</th>
                <th scope="col">Total</th>
                <th scope="col" width="100px">Action</th>
              </tr>
            </thead>
              @foreach ($transaksis->sortByDesc('created_at') as $trans)
              <tr class="text-center">
                <td>{{$trans->invoice}}</td>
                <td>{{$trans->name}}</td>
                <td>{{date('d, M Y H:i' , strtotime($trans->created_at))}}</td>
                <td>{{number_format($trans->amount,0,',','.')}}</td>
                <td>
                      <a role="button" href="{{ route('transaksi.show', $trans->id) }}" class="btn btn-sm btn-icon" data-toggle="tooltip" title="Lihat">
                        <i class="material-icons-outlined">visibility</i>
                      </a>
                      <form action="{{ route('transaksi.destroy', $trans->id) }}" method="POST" class="d-inline" onsubmit="return confirm('Are you sure delete?');">
                        {{ csrf_field() }}
                        {{ method_field('DELETE') }}
                        {{-- <button class="btn btn-sm btn-icon text-muted" data-toggle="tooltip" title="Delete">
                          <i class="material-icons-outlined">delete</i>
                      </button> --}}
                      </form>
                </td>
              </tr>
              @endforeach
            </tbody>
            <tfoot>
              <tr>
                  <td colspan="100">{{ $transaksis->links('administrator.pagination.pagination') }}</td>
              </tr>
          </tfoot>
          </table>
        </div>
      </div>
</div>
@endsection
