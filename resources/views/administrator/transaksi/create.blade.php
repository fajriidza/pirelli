@extends('administrator.header')

@section('isi')
@include('administrator.transaksi.flash-message')
<div class="container" style="padding-top:25px;">  
  <button class="btn btn-warning" onclick="history.back();"><i class="fa fa-arrow-left"></i> Back</button> <br><br> 
  <form action="{{route('cart.add')}}" method="post">
      {{ csrf_field() }}
        {{-- <div class="form-group">
            <label for="date">Tanggal Pembelian</label>
            <input type="text" class="form-control datepicker-here" data-timepicker="true" id="date" name="date" placeholder="Tanggal Pembelian" required>
        </div> --}}
        <div class="form-group">
            <label for="partnumber">Part Number</label>
            <input type="text" class="form-control" id="partnumber" name="partnumber" placeholder="Part Number" required>
        </div>
        <div class="form-group">
            <label for="qty">QTY</label>
            <input type="number" class="form-control" id="qty" name='qty' min="0" placeholder="QTY">
        </div>
        <div class="form-row">
            <div class="form-group col-sm-6">
                <label for="discount_one">Potongan Pertama</label>
                <input type="number" class="form-control" min="0" id="discount_one" name="discount_one" placeholder="Potongan Pertama">
            </div>
            <div class="form-group col-sm-6">
                <label for="discount_two">Potongan Kedua</label>
                <input type="number" class="form-control" min="0" data-timepicker="true" id="discount_two" name="discount_two" placeholder="Potongan Kedua">
            </div>
        </div>
        <button type="submit" class="btn btn-primary">Tambah ke Keranjang</button>
        <a href="{{route('cart.index')}}" class="btn btn-success">Lihat Keranjang</a>
  </form>
</div>
@endsection

@push('css')
<link rel="stylesheet" href="{{ asset('datepicker/css/datepicker.min.css') }}">
<link rel="stylesheet" href="{{ asset('css/easy-autocomplete.min.css') }}">
@endpush

@push('script')
<script src="{{ asset('datepicker/js/datepicker.js') }}"></script>
<script src="{{ asset('datepicker/js/datepicker.en.js') }}"></script>
<script src="{{asset('js/jquery.easy-autocomplete.min.js')}}"></script>
<script>
        $('#partnumber').easyAutocomplete({
            data: @json(App\Product::all('id', 'partnumber')),
            getValue: 'partnumber',
            list: {
                match: { enabled: true },
                maxNumberOfElements: 5,
                onChooseEvent: function () {
                    var selected = $('#partnumber').getSelectedItemData()
                    $('#partnumber').val(selected.partnumber);
                }
            },
        });
$(document).ready(function () {
    $('#date').datepicker({
        language: "en",
        autoClose: true,
        dateFormat: 'yyyy-mm-dd',
        timeformat: 'hh:ii',
    });
    // $('#discount').maskMoney({thousands:'.', decimal:',', precision:0});
    // $('#max_discount').maskMoney({thousands:'.', decimal:',', precision:0,allowZero:true});
    
});
</script>
@endpush