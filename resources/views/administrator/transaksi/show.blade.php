@extends('administrator.header')
@section('isi')
@include('administrator.barang.flash-message')
<div class="container" style="padding-top:25px;">
    <button class="btn btn-warning" onclick="history.back();"><i class="fa fa-arrow-left"></i> Back</button> <br><br> 
    <div class="card">
        <div class="card-body">
          <h5 class="card-title">Detail Pembeli
            <a href="{{route('invoice.print',$transaksi->invoice)}}" target="_blank" class="btn btn-success btn-sm float-right"><i class="material-icons" style="font-size:25px;display:inline-block;">print</i><strong>Print</strong></a>
          </h5>
          <table class="table table-striped table-bordered">
            <thead>
              <tr class="text-center">
                <th scope="col">Kode Transaksi</th>
                <th scope="col">Nama Pembeli</th>
                <th scope="col">Alamat Pembeli</th>
                <th scope="col">Tanggal Pembelian</th>
              </tr>
            </thead>
              <tr class="text-center">
                <td>{{$transaksi->invoice}}</td>
                <td>{{$transaksi->name}}</td>
                <td>{{$transaksi->address}}</td>
                <td>{{date('d, M Y H:i' , strtotime($transaksi->created_at))}}</td>
                </td>
              </tr> 
            </tbody>
          </table>

          <h5 class="card-title" style="padding-top:25px;">Detail Transaksi</h5>
          <table class="table table-striped table-bordered">
            <thead>
              <tr class="text-center">
                <th scope="col">Part Number</th>
                <th scope="col">Pattern</th>
                <th scope="col">Ukuran</th>
                <th scope="col">QTY</th>
                <th scope="col">Harga Het</th>
                <th scope="col" width="100px">Disc</th>
                <th scope="col">Harga Set Discount</th>
                <th scope="col">Total</th>
              </tr>
            </thead>
            @foreach($transaksi->transaksi_details as $item)
               <tr class="text-center">
                <td>{{$item->partnumber}}</td>
                <td>{{$item->product->pattern}}</td>
                <td>{{$item->product->ukuran}}</td>
                <td>{{$item->qty}}</td>
                <td>{{number_format($item->product->het,0,',','.')}}</td>
                @if($item->discount_one==null &&$item->discount_two==null)
                  <td>{{' '}}</td>
                @elseif($item->discount_one!=null &&$item->discount_two==null)
                  <td>{{$item->discount_one.'%'}}</td>
                @elseif($item->discount_one==null &&$item->discount_two!=null)
                  <td>{{$item->discount_two.'%'}}</td>
                  @elseif($item->discount_one!=null &&$item->discount_two!=null)
                  <td>{{$item->discount_one.'% +'.$item->discount_two.'%'}}</td>
                @endif
                <td>{{number_format($item->discounted_price,0,',','.')}}</td>
                <td>{{number_format($item->amount,0,',','.')}}</td>
              </tr>
              @endforeach()
            </tbody>
            <tfoot>
                <tr>
                    <td colspan="5" style="border-left-style:hidden;border-bottom-style:hidden;border-right-style:hidden;">&nbsp;</td>
                    <td style="border-left-style:hidden;border-bottom-style:hidden;">Total</td>
                    <td>{{number_format($transaksi->amount,0,',','.')}}</td>
                </tr>
            </tfoot>
          </table>
        </div>
      </div>
</div>
@endsection
