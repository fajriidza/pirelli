@if(Session::has('notification.error'))
<div style="position:absolute; z-index: 10020; margin-top: 5px; right: 25px;">
  
    <div class="alert alert-danger alert-dismissable fade show" role="alert">
        <i class="fas fa-lg fa-exclamation-circle mr-2"></i>
        {!! \Session::get('notification.error') !!}
        <button style="padding-left:10px" class="close" data-dismiss="alert" aria-label="Close">
            <span aria-hidden="true">&times;</span>
        </button>
    </div>
</div>
@elseif(Session::has('notification.success'))
<div style="position:absolute; z-index: 10020; margin-top: 5px; right: 25px;">
  
    <div class="alert alert-success alert-dismissable fade show" role="alert">
        <i class="fas fa-lg fa-exclamation-circle mr-2"></i>
        {!! \Session::get('notification.success') !!}
        <button style="padding-left:10px" class="close" data-dismiss="alert" aria-label="Close">
            <span aria-hidden="true">&times;</span>
        </button>
    </div>
</div>
 @endif

<script>
window.setTimeout(function() {
    $("div.alert").fadeTo(500, 0).slideUp(500, function(){
        $(this).remove(); 
    });
}, 2500);
</script>