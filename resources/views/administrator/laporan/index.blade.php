@extends('administrator.header')
@section('isi')
<div class="container" style="padding-top:25px;">
  <div class="card">
      <div class="card-body">
        <h5 class="card-title">Product Terjual</h5>
      <form method="get" action="{{route('laporan.index')}}" style="padding-bottom:50px">
        <div class="row">
          <div class="col-md-5">
            <select class="custom-select" id="item_month" name="item_month" required>
              <option value="" disabled selected>Bulan</option>
              <?php
                $bulan=array("Januari","Februari","Maret","April","Mei","Juni","Juli","Agustus","September","Oktober","November","Desember")
              ?>
              @for($i=0; $i<12;$i++)
                <option value="0{{$i+1}}">{{$bulan[$i]}}</option>
              @endfor
            </select>
          </div>
          <div class="col-md-5">
            <select class="custom-select" id="item_year" name="item_year" required>
              <option value="" disabled selected>Tahun</option>
              @for($year=2015; $year<=date('Y'); $year++)
                <option value="{{$year}}">{{$year}}</option>
                @endfor
            </select>
          </div>
          <div class="col-md-2">
            <button type="submit" class="btn btn-primary">Lihat</button>
          </div>
        </div>
           </form>
           <h3 style="text-align:center">Product Terjual di bulan {{ date("F", mktime(0, 0, 0, $month, 1)) }} Tahun {{$tahun}}</h3>

        <div class="row table_wrapper" style="padding-top:1px">
          <table id="table" class="table table-bordered" style="padding-top:25px">
            <thead>
              <tr class="text-center">
                <th scope="col">INVOICE</th>
                <th scope="col">NAMA TOKO</th>
                <th scope="col">TANGGAL</th>
                <th scope="col">PART NUMBER</th>
                <th scope="col">DESC</th>
                <th scope="col">PATTERN</th>
                <th scope="col">NEW HET</th>
                <th scope="col">DISKON</th>
                <th scope="col">HARGA JUAL</th>
                <th scope="col">QTY</th>
                <th scope="col">AMOUNT</th>
                <th scope="col">MDL</th>
                <th scope="col">AMT MDL</th>
                <th scope="col">PROFIT</th>
              </tr>
            </thead>
              @foreach ($transaksis as $t)
              <tr>
                <td>{{$t->invoice}}</td>
                <td>{{$t->name}}</td>
                <td>{{date('d, M Y H:i' , strtotime($t->created_at))}}</td>
                @foreach ($t->transaksi_details as $item)
                  <tr>
                    <td>{{$t->invoice}}</td>
                    <td>{{$t->name}}</td>
                    <td>{{date('d, M Y H:i' , strtotime($t->created_at))}}</td>
                    <td>{{$item->partnumber}}</td>
                    <td>{{$item->product->description}}</td>
                    <td>{{$item->product->pattern}}</td>
                    <td>{{number_format($item->product->het,0,',','.')}}</td>
                    @if($item->discount_one==null &&$item->discount_two==null)
                    <td>{{' '}}</td>
                  @elseif($item->discount_one!=null &&$item->discount_two==null)
                    <td>{{$item->discount_one.'%'}}</td>
                  @elseif($item->discount_one==null &&$item->discount_two!=null)
                    <td>{{$item->discount_two.'%'}}</td>
                    @elseif($item->discount_one!=null &&$item->discount_two!=null)
                    <td>{{$item->discount_one.'% +'.$item->discount_two.'%'}}</td>
                  @endif
                  <td>{{number_format($item->discounted_price,0,',','.')}}</td>
                  <td>{{$item->qty}}</td>
                  <td>{{number_format($item->amount,0,',','.')}}</td>
                  <td>{{number_format($item->product->buy_price,0,',','.')}}</td>
                  <td>{{number_format($item->product->buy_price * $item->qty,0,',','.')}}</td>
                  <td>{{number_format($item->amount - ($item->product->buy_price * $item->qty),0,',','.')}}</td>
                  </tr>
                    
                @endforeach
              </tr>
              @endforeach
            </tbody>
            <tfoot>
          </tfoot>
          </table>
        </div>
        {{ $transaksis->links('administrator.laporan.pagination') }}
      </div>    
      <div>
        <table class="table table-bordered">
          <tr>
            <td><strong>AMT MDL</strong> </td>
            <td><strong>{{number_format($amt_mdl,0,',','.')}}</strong></td>
          </tr>
          <tr>
            <td><strong>AMOUNT</strong> </td>
            <td><strong>{{number_format($amount,0,',','.')}}</strong></td>
          </tr>
          <tr>
            <td><strong>PROFIT </strong></td>
            <td><strong>{{number_format($profit,0,',','.')}}</strong></td>
          </tr>
        </table>
      </div> 
  </div>
</div>
@endsection
@push('script')
<script src="{{asset('js/jquery.rowspanizer.min.js')}}"></script>
<script>
  $('#table').rowspanizer({
    columns: [0,1,2]
});
</script>
@endpush