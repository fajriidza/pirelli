@extends('administrator.header')
@section('isi')
@include('administrator.transaksi.flash-message')
<div class="container" style="padding-top:25px;">  
  <div class="card">
                <div class="card-body">
  <form action="{{route('cart.add')}}" method="post">
      {{ csrf_field() }}
        {{-- <div class="form-group">
            <label for="date">Tanggal Pembelian</label>
            <input type="text" class="form-control datepicker-here" data-timepicker="true" id="date" name="date" placeholder="Tanggal Pembelian" required>
        </div> --}}
        <div class="form-group">
            <label for="partnumber">Part Number</label>
            <input type="text" class="form-control" id="partnumber" name="partnumber" placeholder="Part Number" required>
        </div>
        <div class="form-group">
            <label for="qty">QTY</label>
            <input type="number" class="form-control" id="qty" name='qty' min="0" placeholder="QTY">
        </div>
        <div class="form-row">
            <div class="form-group col-sm-6">
                <label for="discount_one">Potongan Pertama</label>
                <input type="number" class="form-control" min="0" id="discount_one" name="discount_one" placeholder="Potongan Pertama">
            </div>
            <div class="form-group col-sm-6">
                <label for="discount_two">Potongan Kedua</label>
                <input type="number" class="form-control" min="0" data-timepicker="true" id="discount_two" name="discount_two" placeholder="Potongan Kedua">
            </div>
        </div>
        <button type="submit" class="btn btn-primary" style="float:right;">Tambah ke Keranjang</button>
  </form>
</div>
</div>
</div>

<div class="container" style="padding-top:25px;"> 
    <div class="row" style="padding-top:25px;">
        <div class="col-lg-4">
            <div class="card">
                <div class="card-body">
                    <h5 class="card-title">Pembeli</h5>
                    <form id="store-form" action="{{route('cart.store')}}" method="POST">
                      {{ csrf_field() }}
                        <div class="form-group">
                            <label for="name">Nama Pembeli</label>
                            <input type="text" class="form-control" id="name" name="name" placeholder="Nama Pembeli" required>
                        </div>
                        <div class="form-group">
                            <label for="phone">No Hp</label>
                            <input type="number" class="form-control" id="phone" name="phone" placeholder="No Hp" min="0" required>
                        </div>
                        <div class="form-group">
                            <label for="address">Alamat Pembeli</label>
                            <textarea name="address" id="description" rows="5" class="form-control" placeholder="Alamat Pembeli"></textarea>
                        </div>
                    </form> 
                </div>
            </div>
      </div>
      <div class="col-lg-8">
      <div class="card">
        <div class="card-body">
          <h5 class="card-title">Cart</h5>
          <table class="table table-striped table-bordered">
            <thead>
              <tr class="text-center">
                <th scope="col">PATTERN</th>
                <th scope="col">QTY</th>
                <th scope="col">Harga Het</th>
                <th scope="col">Disc</th>
                <th scope="col">Harga Set Discount</th>
                <th scope="col">Total</th>
                <th scope="col"  width="10px">Action</th>
              </tr>
            </thead>
            <tbody>
              @foreach ($cartItems as $cartItem)
              <tr>
                <td>{{$cartItem->name}}</td>
                <td>{{$cartItem->qty}}</td>
                <td>{{number_format($cartItem->options['product_het'],0,',','.')}}</td>
                  @if($cartItem->options['discount_one']==null && $cartItem->options['discount_two']==null)
                  <td>{{' '}}</td>
                  @elseif($cartItem->options['discount_one']!=null &&$cartItem->options['discount_two']==null)
                  <td>{{$cartItem->options['discount_one']}}</td>
                  @elseif($cartItem->options['discount_one']==null &&$cartItem->options['discount_one']!=null)
                  <td>{{$cartItem->options['discount_two']}}</td>
                  @elseif($cartItem->options['discount_one']!=null &&$cartItem->options['discount_two']!=null)
                  <td>{{$cartItem->options['discount_one'].'+'.$cartItem->options['discount_two']}}</td>
                  @endif
                <td>{{number_format($cartItem->price,0,',','.')}}</td>
                <td>{{number_format($cartItem->qty*$cartItem->price,0,',','.')}}</td>
                <td>   <form action="{{ route('cart.destroy', $cartItem->rowId) }}" method="POST" class="d-inline" onsubmit="return confirm('Are you sure delete?');">
                    {{ csrf_field() }}
                    {{ method_field('DELETE') }}
                    <button class="btn btn-sm btn-icon text-muted" data-toggle="tooltip" title="Delete">
                      <i class="material-icons-outlined">delete</i>
                  </button>
                  </form>
                </td>
              </tr>
              @endforeach
            </tbody>
            <tfoot>
                <tr>
                    <td colspan="4" style="border-left-style:hidden;">&nbsp;</td>
                    <td style="border-left-style:hidden;">Total</td>
                    <td><?php echo Cart::subtotal(); ?></td>
                </tr>
                <tr>
                  <td colspan="5"  style="border-style:hidden;"></td>
                  <td style="border-left-style:hidden; border-bottom-style:hidden; border-right-style:hidden;"></td>
                  <td colspan="1" style="border-style:hidden;">&nbsp;<a href="{{route('cart.store')}}" onclick="event.preventDefault();
                    document.getElementById('store-form').submit();" class="btn  btn-md btn-primary">Process</button></td>
              </tr>
            </tfoot>
          </table>
        </div>
      </div>
      </div>
    </div> 
</div>
@endsection

@push('css')
<link rel="stylesheet" href="{{ asset('datepicker/css/datepicker.min.css') }}">
<link rel="stylesheet" href="{{ asset('css/easy-autocomplete.min.css') }}">
@endpush

@push('script')
<script src="{{ asset('datepicker/js/datepicker.js') }}"></script>
<script src="{{ asset('datepicker/js/datepicker.en.js') }}"></script>
<script src="{{asset('js/jquery.easy-autocomplete.min.js')}}"></script>
<script>
        $('#partnumber').easyAutocomplete({
            data: @json(App\Product::all('id', 'partnumber')),
            getValue: 'partnumber',
            list: {
                match: { enabled: true },
                maxNumberOfElements: 5,
                onChooseEvent: function () {
                    var selected = $('#partnumber').getSelectedItemData()
                    $('#partnumber').val(selected.partnumber);
                }
            },
        });
</script>
@endpush
