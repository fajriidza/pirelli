@extends('administrator.header')
@section('isi')
@include('administrator.barang.flash-message')
<div class="container" style="padding-top:25px;">
    <div class="card">
        <div class="card-body">
          <h5 class="card-title">Barang</h5>
          <table class="table table-striped table-bordered">
            <thead>
              <tr class="text-center">
                <th scope="col">Part Number</th>
                <th scope="col">Desc</th>
                <th scope="col">F/R</th>
                <th scope="col">TL/TT</th>
                <th scope="col">Pattern</th>
                <th scope="col">Type</th>
                <th scope="col">HET</th>
                <th scope="col">STOK</th>
              </tr>
            </thead>
              @foreach ($barangs as $bar)
              <tr>
                <td>{{$bar->partnumber}}</td>
                <td>{{$bar->description}}</td>
                <td>{{$bar->fr}}</td>
                <td>{{$bar->tltt}}</td>
                <td>{{$bar->pattern}}</td>
                <td>{{$bar->type}}</td>
                <td>{{$bar->het}}</td>
                <td>{{$bar->stok}}</td>
              </tr>
              @endforeach
            </tbody>
          </table>
        </div>
      </div>
</div>
@endsection
