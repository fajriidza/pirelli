@extends('administrator.header')

@section('isi')
<div class="container" style="padding-top:25px;padding-bottom:25px;">  
  <button class="btn btn-warning" onclick="history.back();"><i class="fa fa-arrow-left"></i> Kembali</button>  
  <form action="{{route('barang.stok.store')}}" method="post">
      {{ csrf_field() }}
        <div class="form-group">
        <label for="partnumber">Part Number</label>
        <input type="text" class="form-control" id="partnumber" name="partnumber" required>
        </div>
        <div id="detail" style="display:none;">
          <div class="form-group">
          <label for="pattern">Pattern</label>
          <input type="text" class="form-control" id="pattern" name="pattern" readonly>
          </div>

        <div class="form-group">
            <label for="sisa_stok"> Sisa Stok</label>
            <input type="number" class="form-control" id="sisa_stok" name='sisa_stok' min="1" placeholder="unit" readonly>
        </div>
      
        <div class="form-group">
            <label for="stok">Tambah Stok</label>
            <input type="number" class="form-control" id="tambah_stok" name='tambah_stok' min="1" placeholder="unit" required>
        </div>
        <button type="submit" class="btn btn-primary">Tambah</button>
        </div>
  </form>
</div>
@endsection

@push('css')
<link rel="stylesheet" href="{{ asset('css/easy-autocomplete.min.css') }}">
@endpush

@push('script')
<script src="{{asset('js/jquery.easy-autocomplete.min.js')}}"></script>
<script src="{{asset('js/jquery.maskMoney.min.js')}}"></script>
<script>
$(document).ready(function () {

     $('#het').maskMoney({thousands:'.', decimal:',', precision:0});
     $('#buy_price').maskMoney({thousands:'.', decimal:',', precision:0});
    
});
        $('#partnumber').easyAutocomplete({
            data: @json(App\Product::all()),
            getValue: 'partnumber',
            list: {
                match: { enabled: true },
                maxNumberOfElements: 5,
                onChooseEvent: function () {
                    $('#detail').show();
                    var selected = $('#partnumber').getSelectedItemData()
                    $('#partnumber').val(selected.partnumber);
                    $('#sisa_stok').val(selected.stok);
                    $('#pattern').val(selected.pattern);
                    
                }
            },
        });
</script>
@endpush