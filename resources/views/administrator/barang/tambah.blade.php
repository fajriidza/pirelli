@extends('administrator.header')

@section('isi')
<div class="container" style="padding-top:25px;padding-bottom:25px;">  
  <button class="btn btn-warning" onclick="history.back();"><i class="fa fa-arrow-left"></i> Kembali</button>  
  <form action="{{route('barang.store')}}" method="post">
      {{ csrf_field() }}
        <div class="form-group">
        <label for="partnumber">Part Number</label>
        <input type="text" class="form-control" id="partnumber" name="partnumber" required>
        </div>
        <div class="form-group">
        <label for="Description">Description</label>
        <input type="text" class="form-control" id="description" name="description" required>
        </div>

        <div class="input-group mb-3">
            <div class="input-group-prepend">
              <label class="input-group-text" for="inputGroupSelect01">F/R</label>
            </div>
            <select class="custom-select" id="inputGroupSelect01" name="fr">
              <option selected>Pilih...</option>
              <option value="F">F</option>
              <option value="R">R</option>
            </select>
          </div>

          <div class="form-group">
            <label for="Pattern">Pattern</label>
            <input type="text" class="form-control" id="Pattern" name="pattern" required>
          </div>
          <div class="form-group">
            <label for="ukuran">Ukuran</label>
            <input type="text" class="form-control" id="ukuran" name="ukuran" required>
          </div>

        <div class="input-group mb-3">
            <div class="input-group-prepend">
              <label class="input-group-text" for="inputGroupSelect01">TL/TT</label>
            </div>
            <select class="custom-select" id="inputGroupSelect01" name="tltt">
              <option selected>Pilih...</option>
              <option value="TL">TL</option>
              <option value="TT">TT</option>
            </select>
        </div>
        <div class="input-group mb-3">
            <div class="input-group-prepend">
              <label class="input-group-text" for="inputGroupSelect01">TYPE</label>
            </div>
            <select class="custom-select" id="inputGroupSelect01" name="type">
              <option selected>Pilih...</option>
              <option value="Radial">Radial</option>
              <option value="Radialino">Radialino</option>
            </select>
        </div>
        <div class="form-group">
          <label for="buy_price">Harga Beli</label>
          <input type="text" class="form-control mask-money" id="buy_price" name='buy_price' placeholder="RP...">
        </div>
        <div class="form-group">
            <label for="het">HET</label>
            <input type="text" class="form-control mask-money" id="het" name='het' placeholder="RP...">
          </div>
        <div class="form-group">
            <label for="exampleInputEmail1">Stok</label>
            <input type="number" class="form-control" id="stok" name='stok' min="1" placeholder="unit">
        </div>

        <button type="submit" class="btn btn-primary">Tambah</button>
  </form>
</div>
@endsection

@push('script')
<script src="{{asset('js/jquery.maskMoney.min.js')}}"></script>
<script>
$(document).ready(function () {

     $('#het').maskMoney({thousands:'.', decimal:',', precision:0});
     $('#buy_price').maskMoney({thousands:'.', decimal:',', precision:0});
    
});
</script>
@endpush