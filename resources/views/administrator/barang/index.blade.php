@extends('administrator.header')
@section('isi')
@include('administrator.barang.flash-message')
<div class="container" style="padding-top:25px;">
    <div class="card">
        <div class="card-body">
          <h5 class="card-title">Barang
            <a href="{{route('add.stok')}}" style="margin-left:10px;" class="btn btn-primary btn-sm float-right">Tambah Stok</a>
            <a href="{{route('barang.create')}}" class="btn btn-primary btn-sm float-right">Tambah Barang</a>
          </h5>
          <table class="table table-striped table-bordered">
            <thead>
              <tr class="text-center">
                <th scope="col">Part Number</th>
                <th scope="col">Desc</th>
                <th scope="col">F/R</th>
                <th scope="col">TL/TT</th>
                <th scope="col">Pattern</th>
                <th scope="col">Ukuran</th>
                <th scope="col">Type</th>
                <th scope="col">Harga Beli</th>
                <th scope="col">HET</th>
                <th scope="col">STOK</th>
                <th scope="col">Action</th>
              </tr>
            </thead>
              @foreach ($barangs as $bar)
              <tr>
                <td>{{$bar->partnumber}}</td>
                <td>{{$bar->description}}</td>
                <td>{{$bar->fr}}</td>
                <td>{{$bar->tltt}}</td>
                <td>{{$bar->pattern}}</td>
                <td>{{$bar->ukuran}}</td>
                <td>{{$bar->type}}</td>
                <td>{{number_format($bar->buy_price,0,',','.')}}</td>
                <td>{{number_format($bar->het,0,',','.')}}</td>
                <td>{{$bar->stok}}</td>
                <td>
                    <div class="btn-group" role="group" aria-label="Basic example">
                      <a role="button" href="{{ route('barang.edit', $bar->id) }}" class="btn btn-sm btn-icon text-muted" data-toggle="tooltip" title="Edit">
                        <i class="material-icons-outlined">edit</i>
                      </a>
                      <form action="{{ route('barang.destroy', $bar->id) }}" method="POST" class="d-inline" onsubmit="return confirm('Are you sure delete?');">
                        {{ csrf_field() }}
                        {{ method_field('DELETE') }}
                        <button class="btn btn-sm btn-icon text-muted" data-toggle="tooltip" title="Delete">
                          <i class="material-icons-outlined">delete</i>
                      </button>
                      </form>
                    </div>
                </td>
              </tr>
              @endforeach
            </tbody>
            <tfoot>
              <tr>
                  <td colspan="100">{{ $barangs->links('administrator.pagination.pagination') }}</td>
              </tr>
          </tfoot>
          </table>
        </div>
      </div>
</div>
@endsection
