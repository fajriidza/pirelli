@extends('administrator.header')

@section('isi')
<div class="container" style="padding-top:25px;">  
  <button class="btn btn-warning" onclick="history.back();"><i class="fa fa-arrow-left"></i> Kembali</button>  
  <form action="{{route('barang.update',$barang->id)}}" method="post">
      {{ csrf_field() }}
      {{ method_field('PUT') }}
        <div class="form-group">
        <label for="Part Number">Part Number</label>
        <input type="text" class="form-control" id="partnumber" name="partnumber" value="{{$barang->partnumber}}" required>
        </div>
        <div class="form-group">
        <label for="Description">Description</label>
        <input type="text" class="form-control" id="description" name="description" value="{{$barang->description}}" required>
        </div>

        <div class="input-group mb-3">
            <div class="input-group-prepend">
              <label class="input-group-text" for="inputGroupSelect01">F/R</label>
            </div>
            <select class="custom-select" id="inputGroupSelect01" name="fr">
            <option selected hidden value="{{$barang->fr}}">{{$barang->fr}}</option>
              <option value="F">F</option>
              <option value="R">R</option>
            </select>
          </div>

          <div class="form-group">
            <label for="Pattern">Pattern</label>
            <input type="text" class="form-control" id="Pattern" name="pattern" value="{{$barang->pattern}}" required>
          </div>
          <div class="form-group">
            <label for="ukuran">Ukuran</label>
            <input type="text" class="form-control" id="ukuran" name="ukuran" value="{{$barang->ukuran}}" required>
          </div>

        <div class="input-group mb-3">
            <div class="input-group-prepend">
              <label class="input-group-text" for="inputGroupSelect01">TL/TT</label>
            </div>
            <select class="custom-select" id="inputGroupSelect01" name="tltt">
              <option selected hidden value="{{$barang->tltt}}">{{$barang->tltt}}</option>
              <option value="TL">TL</option>
              <option value="TT">TT</option>
            </select>
        </div>
        <div class="input-group mb-3">
            <div class="input-group-prepend">
              <label class="input-group-text" for="inputGroupSelect01">TYPE</label>
            </div>
            <select class="custom-select" id="inputGroupSelect01" name="type">
              <option selected hidden value="{{$barang->type}}">{{$barang->type}}</option>
              <option value="Radial">Radial</option>
              <option value="Radialino">Radialino</option>
            </select>
        </div>
        <div class="form-group">
          <label for="buy_price">Harga Beli</label>
          <input type="text" class="form-control mask-money" id="buy_price" name='buy_price' placeholder="RP..." value="{{number_format($barang->buy_price,0,',','.')}}">
        </div>
        <div class="form-group">
            <label for="het">HET</label>
            <input type="text" class="form-control mask-money" id="het" name='het' placeholder="RP..." value="{{number_format($barang->het,0,',','.')}}">
          </div>
        <div class="form-group">
            <label for="exampleInputEmail1">Stok</label>
            <input type="number" class="form-control" id="stok" name='stok' placeholder="unit" value="{{$barang->stok}}">
        </div>

        <button type="submit" class="btn btn-primary">Simpan</button>
  </form>
</div>
@endsection
@push('script')
<script src="{{asset('js/jquery.maskMoney.min.js')}}"></script>
<script>
$(document).ready(function () {

     $('#het').maskMoney({thousands:'.', decimal:',', precision:0});
     $('#buy_price').maskMoney({thousands:'.', decimal:',', precision:0});
    
});
</script>
@endpush