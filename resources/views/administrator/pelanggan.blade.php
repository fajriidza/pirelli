@extends('administrator.header')
@section('isi')
<div class="container">
    <div class="card">
        <div class="card-body">
          <h5 class="card-title">Pelanggan
              <a href="tambah" class="btn btn-primary btn-sm float-right">Tambah Pelanggan</a>
          </h5>
          <table class="table table-striped table-bordered">
            <thead>
              <tr class="text-center">
                <th scope="col">#</th>
                <th scope="col">id Pelanggan</th>
                <th scope="col">Nama Toko</th>
                <th scope="col">Nomor Telepon</th>
                <th scope="col">Alamat</th>
                <th scope="col">Nama Pemilik</th>
                <th scope="col">Action</th>
              </tr>
            </thead>
            <tbody>
              <tr>
                <th scope="row">1</th>
                <td>2561600</td>
                <td>DFD Tech</td>
                <td>08217364531</td>
                <td>Jl Dunia</td>
                <td>Deni Farjun Daru</td>
                <td>
                    <div class="btn-group btn-block" role="group" aria-label="Basic example">
                        <a href="" class="btn btn-sm btn-warning">Edit</a> <a class='btn btn-sm btn-danger' href="">Hapus</a></td>   
                      </div>
              </tr>
            </tbody>
          </table>
        </div>
      </div>
</div>
@endsection
