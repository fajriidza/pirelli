<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableBarang extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('products', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('partnumber', 8)->index();
            $table->text('description');
            $table->enum('fr',['F','R']);
            $table->enum('tltt',['TL','TT']);
            $table->string('pattern',30);
            $table->string('ukuran',30);
            $table->enum('type',['Radial','Radialino']);
            $table->integer('buy_price');
            $table->integer('het');
            $table->integer('stok');
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('products');
    }
}
