<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class TableTransaksiDetails extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('transactions_details', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('transaction_id')->index();
            $table->unsignedBigInteger('product_id')->index();
            $table->string('partnumber',8)->index();
            $table->unsignedInteger('qty');
            $table->unsignedInteger('discount_one')->nullable();
            $table->unsignedInteger('discount_two')->nullable();
            $table->decimal('discounted_price',32,0);
            $table->decimal('amount',32,0);
            $table->timestamps();
            $table->softDeletes();

            $table->foreign('product_id')->references('id')->on('products')
                ->onDelete('cascade')->onUpdate('cascade');
                $table->foreign('transaction_id')->references('id')->on('transactions')
                ->onDelete('cascade')->onUpdate('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('transactions_details');
    }
}
