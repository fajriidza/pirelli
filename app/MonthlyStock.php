<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class MonthlyStock extends Model
{
    protected $table ='monthly_stock';

    protected $fillable = ['product_id','stok'];

    public function product()
    {
        return $this->hasOne(Product::class, 'id','product_id');
    }
}
