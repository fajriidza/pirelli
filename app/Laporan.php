<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\TransaksiDetails;
use App\Transaksi;

class Laporan extends Model
{
    public static function getTotalAmtMdl($month, $year)
    {
        $details = TransaksiDetails::whereMonth('created_at',$month)
                ->whereYear('created_at',$year)->get();
                
        $total_amt_mdl =0;

        foreach($details as $item){
            $total_amt_mdl = $total_amt_mdl + ($item->product->buy_price * $item->qty);
        }

        return $total_amt_mdl;
    }

    public static function getTransaksi($month, $year)
    {
        $transaksis = Transaksi::orderBy('created_at','DESC')
        ->whereMonth('created_at',$month)
        ->whereYear('created_at',$year)
        ->paginate(10);

        return $transaksis;

    }
}
