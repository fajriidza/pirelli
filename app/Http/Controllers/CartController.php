<?php

namespace App\Http\Controllers;

use App\Product;
use App\Transaksi;
use App\TransaksiDetails;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Gloudemans\Shoppingcart\Facades\Cart;

class CartController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $cartItems = Cart::content();
        //dd(Cart::content());

        return view('administrator.cart.index',compact('cartItems'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'name' => ['required', 'string', 'max:200'],
            'phone' => ['required', 'numeric', 'min:10'],
            'address' => 'sometimes',
        ]);

        DB::transaction(function () use ($request) {
            $invoice = date('my',strtotime(now())).rand(1000,9999);
            $amount = str_replace('.', '', Cart::subtotal());
            $cartItems = Cart::content();
            //dd($cartItems);
            $transaksi = Transaksi::create(array_merge($request->all(), 
                         ['invoice'=>$invoice, 'amount'=>$amount]));
            foreach($cartItems as $cartItem){
                $product = Product::where('id',$cartItem->id)->firstOrfail();
                TransaksiDetails::create([
                    'transaction_id'   => $transaksi->id,
                    'product_id'       => $product->id,
                    'partnumber'       => $product->partnumber,
                    'qty'              => $cartItem->qty,
                    'discount_one'     => $cartItem->options['discount_one'],
                    'discount_two'     => $cartItem->options['discount_two'],
                    'discounted_price' => $cartItem->price,
                    'amount'           => $cartItem->qty*$cartItem->price
                    
                ]);
                $product->update(['stok' => $product->stok-$cartItem->qty]);
            }
        });
        Cart::destroy();
        return redirect()->route('transaksi.index')->with('notification.success', trans('Transaksi Berhasil.'));;

    }

    public function add(Request $request)
    {
        $request->validate([
            'partnumber' => ['required','numeric','min:0'],
            'qty' => 'required|numeric|min:1',
            'discount_one' => ['sometimes', 'nullable', 'numeric','min:0'],
            'discount_two' => ['sometimes', 'nullable', 'numeric','min:0'],
        ]);
        $product = Product::where('partnumber',$request->partnumber)->firstOrfail();
        if($product->stok<$request->qty){
            return redirect()->route('cart.index')->with('notification.error', trans('Stok tidak Mencukupi.'));
        }else{
            Cart::add(['id'=>$product->id,'qty'=>$request->qty,'price'=>$product->getDiscounted_price($request->discount_one,$request->discount_two),'name'=>$product->pattern,'price_discount'=>$product->het,'options'=>['discount_one'=>$request->discount_one,'discount_two'=>$request->discount_two,'product_het'=>$product->het]]);
            return redirect()->route('cart.index')->with('notification.success', trans('Berhasil di tambahkan ke keranjang.'));
        }
    }

    public function remove()
    {
        Cart::remove();
        return back();
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
       
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Cart::remove($id);
        return back();
    }
}
