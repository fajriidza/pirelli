<?php

namespace App\Http\Controllers;
use App\Product;
use Illuminate\Http\Request;

class gudangController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $barangs = Product::all();

        return view('administrator.dashboard.index',compact('barangs'));
    }  
}
