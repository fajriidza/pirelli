<?php

namespace App\Http\Controllers;

use App\Product;
use App\MonthlyStock;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Http\Requests\BarangStoreRequest;


class barangController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $barangs = Product::paginate(10);

        return view('administrator.barang.index',compact('barangs'));
    }    

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('administrator.barang.tambah');
    }
      
    /**
     * Store barang to database.
     *
     * @param \App\Http\Requests\BarangStoreRequest $request
     * @return \Illuminate\Http\Response
     */
    public function store(BarangStoreRequest $request)
    {
        DB::transaction(function () use ($request) {
            /** @var \App\Product $barang */
            $het = str_replace('.', '', $request->het); 
            $buy_price = str_replace('.', '', $request->buy_price); 
            $barang = Product::create(
                    array_merge($request->validated(), ['het'=>$het, 'buy_price'=>$buy_price])
            );
            MonthlyStock::create([
                'product_id' => $barang->id,
                'stok' => $request->stok
            ]);

        });

        return redirect()->route('barang.index');
    }

            /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Barang  $barang
     * @return \Illuminate\Http\Response
     */
    public function edit(Product $barang)
    {
        return view('administrator.barang.edit', compact('barang'));
    }

            /**
     * Update the specified resource in storage.
     *
     * @param  \App\Http\Requests\BarangStoreRequest  $request
     * @param  \App\Barang $barang
     * @return \Illuminate\Http\Response
     */
    public function update(BarangStoreRequest $request, Product $barang)
    {
        DB::transaction(function () use ($barang, $request) {
            /** @var \App\Barang $barang */
            $het = str_replace('.', '', $request->het); 
            $buy_price = str_replace('.', '', $request->buy_price); 
            $barang->update(array_merge($request->validated(), ['het'=>$het, 'buy_price'=>$buy_price])
                    );
        });

        return redirect()->route('barang.index')->with('notification.success', trans('Barang berhasil di update.'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Product  $barang
     * @return \Illuminate\Http\Response
     */
    public function destroy(Product $barang)
    {
        $barang->delete();

        return redirect()->route('barang.index')->with('notification.success', trans('Barang berhasil di hapus.'));
    }

    public function addStock()
    {
        return view('administrator.barang.stok');
    }

    public function storeStock(Request $request)
    {
        $barang = Product::where('partnumber',$request->partnumber)->first();
        DB::transaction(function () use ($barang, $request) {
            /** @var \App\Barang $barang */
            $stok = $barang->stok + $request->tambah_stok;
            $barang->update(['stok'=> $stok
                        ]);
            MonthlyStock::create([
                'product_id' => $barang->id,
                'stok' => $request->tambah_stok
                ]);
            
        });

        return redirect()->route('barang.index')->with('notification.success', trans('Stok barang berhasil di tambah.'));
    }
    
}
