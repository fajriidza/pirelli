<?php

namespace App\Http\Controllers;

use PDF;
use App\Barang;
use App\Transaksi;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class transaksiController extends Controller
{
        /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $transaksis = Transaksi::orderBy('created_at','DESC')->paginate(10);
        return view('administrator.transaksi.index',compact('transaksis'));
    }  

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('administrator.transaksi.create');
    }

        /**
     * Store Transaksi to database.
     *
     * @param \App\Http\Requests\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
       $request->validate([
           'date' => ['required', 'date', 'date_format:Y-m-d H:i'],
           'partnumber' => ['required','numeric','min:0'],
           'name' => ['required', 'string', 'max:200'],
           'phone' => ['required', 'numeric', 'min:10'],
           'address' => 'sometimes',
           'qty' => 'required|numeric|min:1',
           'discount_one' => ['sometimes', 'nullable', 'numeric','min:0'],
           'discount_two' => ['sometimes', 'nullable', 'numeric','min:0'],
       ]);

       $barang = Product::where('partnumber',$request->partnumber)->firstOrFail();
       if($barang->stok<$request->qty){
        return redirect()->route('transaksi.create')->with('notification.error', trans('Stok tidak Mencukupi.'));
       }else{
            DB::transaction(function () use ($request) {
                /** @var \App\Transaksi $transaksi */
                $barang = Barang::where('partnumber',$request->partnumber)->firstOrFail();
                $transaksi = Transaksi::create([
                    'invoice' => $barang->getInvoice(),
                    'partnumber_id' => $request->partnumber,
                    'name' => $request->name,
                    'phone' => $request->phone,
                    'address' => $request->address,
                    'qty' => $request->qty,
                    'date' => $request->date,
                    'discount_one' => $request->discount_one,
                    'discount_two' => $request->discount_two,
                    'discounted_price' => $barang->getDiscounted_price($request->discount_one,$request->discount_two),
                    'amount' => $barang->getDiscounted_price($request->discount_one,$request->discount_two) * $request->qty
                ]);
                $barang->update(['stok' => $barang->stok-$request->qty]);
                return $transaksi;
            });
            return redirect()->route('transaksi.index');    
       }
       
    }

    /**
     * Show the form.
     *
     * @param  $partnumber
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $transaksi = Transaksi::where('id',$id)->firstOrfail();
        return view('administrator.transaksi.show', compact('transaksi'));
    }

    public function generatePDF($invoice)
    {
        
        $transaksi = Transaksi::where('invoice',$invoice)->firstOrfail();
        $pdf = PDF::loadView('administrator.transaksi.invoice', compact('transaksi'))->setPaper('a4','landscape');
        return $pdf->stream();

    }
    
}
