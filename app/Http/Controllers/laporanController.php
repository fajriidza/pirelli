<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Product;
use App\MonthlyStock;
use App\Transaksi;
use App\TransaksiDetails;
use App\Laporan;
use DB;

class laporanController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

     public function index(Request $request)
     {

        if($request->item_month && $request->item_year){
                    $month = $request->item_month;
                    $tahun = $request->item_year;
                    
                }else{
                    $month = date('m');
                    $tahun = date('yy');
            
                }
                $transaksis = Laporan::getTransaksi($month,$tahun);

                $amt_mdl = Laporan::getTotalAmtMdl($month,$tahun);
                $amount = $transaksis->sum('amount');
                $profit = $amount - $amt_mdl;


        return view('administrator.laporan.index',compact('profit','amount','amt_mdl','month','tahun','transaksis'));
     }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
