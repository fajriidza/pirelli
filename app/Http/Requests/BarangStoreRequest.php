<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class BarangStoreRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'partnumber' => ['required','numeric','min:0'],
            'description' => ['required','string'],
            'fr' => ['required','in:F,R'],
            'tltt' => ['required','in:TL,TT'],
            'pattern' => ['required'],
            'ukuran' => ['required'],
            'type' => ['required','in:Radial,Radialino'],
            'buy_price' => ['required','min:1'],
            'het' => ['required','min:1'],
            'stok' => ['required','numeric','min:0'],
        ];
    }
}
