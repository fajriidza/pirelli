<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Transaksi extends Model
{
    protected $table = 'transactions';
    
    protected $fillable = ['invoice', 'name','phone','address',
                           'qty','amount'
                        ];
    public function getTanggalAttribute()
    {
        return \Carbon\Carbon::parse($this->attributes['date'])
            ->format('d, M Y H:i');
    }

    public function transaksi_details()
    {
        return $this->hasMany(TransaksiDetails::class, 'transaction_id','id');
    }
}
