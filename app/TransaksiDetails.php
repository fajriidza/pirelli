<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class TransaksiDetails extends Model
{
    protected $table = 'transactions_details';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['transaction_id', 'product_id', 'partnumber',
                           'qty', 'discount_one', 'discount_two', 
                           'discounted_price', 'amount'
                        ];

    protected $guarded = [];
    public function getTanggalAttribute()
    {
        return \Carbon\Carbon::parse($this->attributes['date'])
            ->format('d, M Y H:i');
    }

    public function product()
    {
        return $this->hasOne(Product::class, 'id','product_id');
    }

    public function transaksi()
    {
        return $this->hasOne(Transaksi::class,'id','transaction_id');
    }
}
