<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Product extends Model
{
    protected $table = 'products';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'partnumber','description','fr','tltt','pattern','ukuran','type', 'buy_price', 'het','stok'
    ];

    public function getInvoice()
    {
        return date('my',strtotime(now())).rand(1000,9999);
    }

    public function getDiscounted_price($dis1,$dis2)
    {
        if($dis1==0){
            $dis=null;
        }
        if($dis2==0){
            $dis=null;
        }
        if($dis1==null && $dis2==null){
            return $this->het;
        }
        elseif($dis1 !=null && $dis2==null){
            $discount = $this->het-($dis1 / 100 * $this->het);
        }
        elseif($dis1 ==null && $dis2!=null){
            $discount = $this->het-($dis2 / 100 * $this->het);
        }
        elseif($dis1!=null && $dis2!=null){
            $discount1 = $this->het - ($dis1 /100 * $this->het);
            $discount = $discount1-($dis2 / 100 * $discount1);
        }
        return $discount;
    }

    
    
}
